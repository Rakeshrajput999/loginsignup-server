var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken')
const fs = require('fs')
const events = require('../db/events')
const verifyToken =require('../middlewares/AuthTOken.js')



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/dashboard', verifyToken, (req, res) => {
  jwt.verify(req.token, 'the_secret_key', err => {
    if (err) {
      res.sendStatus(401)
    } else {
      res.json({
        events: events
      })
    }
  })
})

router.post('/register', (req, res) => {
  if (req.body) {
    const user = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
      // In a production router, you'll want to encrypt the password
    }
    const data = JSON.stringify(user, null, 2)

    var dbUserEmail = require('../db/user.json').email
    var errorsToSend = []

    if (dbUserEmail === user.email) {
      errorsToSend.push('An account with this email already exists.')
    }
    if (user.password.length < 5) {
      errorsToSend.push('Password too short.')
    }
    if (errorsToSend.length > 0) {
      res.status(400).json({ errors: errorsToSend })
    } else {
      fs.writeFile('./db/user.json', data, err => {
        if (err) {
          console.log(err + data)
        } else {
          const token = jwt.sign({ user }, 'the_secret_key')
          // In a production router, you'll want the secret key to be an environment variable
          res.json({
            token,
            email: user.email,
            name: user.name
          })
        }
      })
    }
  } else {
    res.sendStatus(400)
  }
})

router.post('/login', (req, res) => {
  const userDB = fs.readFileSync('./db/user.json')
  const userInfo = JSON.parse(userDB)
  if (
    req.body &&
    req.body.email === userInfo.email &&
    req.body.password === userInfo.password
  ) {
    const token = jwt.sign({ userInfo }, 'the_secret_key')
    // In a production router, you'll want the secret key to be an environment variable
    res.json({
      token,
      email: userInfo.email,
      name: userInfo.name
    })
  } else {
    res.status(401).json({ error: 'Invalid login. Please try again.' })
  }
})



module.exports = router;
